# Try to find OccWrapper
# Once done, this will define:
# TOPOMANAGERS_FOUND - System has OccWrapper
# TOPOMANAGERS_INCLUDE_DIRS - The OccWrapper include directories

set (TOPOMANAGERS_FOUND "NO")

find_path(TOPOMANAGERS_INCLUDE_DIR SolidManagers.hpp
    PATH_SUFFIXES include TopoManagers)

find_library(TOPOMANAGERS_LIBRARY 
    NAMES TopoManagers 
    PATH_SUFFIXES lib)

get_filename_component(TOPOMANAGERS_LIBRARIES_DIR ${TOPOMANAGERS_LIBRARY} DIRECTORY CACHE)

include(FindPackageHandleStandardArgs) # to handle standard args, provided by cMake (?)
find_package_handle_standard_args(TopoManagers DEFAULT_MSG
                                  TOPOMANAGERS_INCLUDE_DIR TOPOMANAGERS_LIBRARY)
if (TOPOMANAGERS_FOUND)
    message("-- Found TopoManagers")
    message("-- -- TopoManagers include directory = ${TOPOMANAGERS_INCLUDE_DIR}")
    message("-- -- TopoManagers library directory = ${TOPOMANAGERS_LIBRARIES_DIR}")
endif(TOPOMANAGERS_FOUND)

mark_as_advanced(TOPOMANAGERS_INCLUDE_DIR TOPOMANAGERS_LIBRARY)
set(TOPOMANAGERS_INCLUDE_DIRS ${TOPOMANAGERS_INCLUDE_DIR} CACHE PATH "The path to TopoManagers headers")
set(TOPOMANAGERS_FOUND ${TOPOMANAGERS_FOUND})
