# Try to find OPENCASCADE
# Once done, this will define:
# OPENCASCADE_FOUND - System has OpenCasCade
# OPENCASCADE_INCLUDE_DIRS - The OpenCasCade include directories
# OPENCASCADE_LIBRARIES - The libraries needed to use OpenCasCade

set (OPENCASCADE_FOUND OFF)

# Create some search path hints
file(GLOB search1 /lib/opencascade*)
file(GLOB search2 /usr/lib/opencascade*)
file(GLOB search3 /local/usr/lib/opencascade*)
set(PATH_HINTS ${search1} ${search2} ${search3})

# first try to find headers
set(SUFFIXES inc oce opencascade occt include "include/opencascade")
find_path(OPENCASCADE_INCLUDE_DIR TopoDS_Shape.hxx
    HINTS ${PATH_HINTS}
    PATH_SUFFIXES ${SUFFIXES} 
)

# now try to find libraries
find_library(
    OPENCASCADE_LIBRARY 
    NAMES TKBRep
    HINTS ${PATH_HINTS}
    PATH_SUFFIXES lib
)
get_filename_component(OPENCASCADE_LIBRARIES_DIR ${OPENCASCADE_LIBRARY} DIRECTORY CACHE)
# now, handle the appropriate arguments
include(FindPackageHandleStandardArgs) # to handle standard args, provided by cMake (?)
find_package_handle_standard_args(
    OPENCASCADE 
    DEFAULT_MSG
    OPENCASCADE_INCLUDE_DIR 
    OPENCASCADE_LIBRARY
)

if (OPENCASCADE_FOUND)
    message("-- Found OpenCasCade")
    message("-- OpenCasCade include directory = ${OPENCASCADE_INCLUDE_DIR}")
    message("-- OpenCasCade library directory = ${OPENCASCADE_LIBRARIES_DIR}")
endif(OPENCASCADE_FOUND)

mark_as_advanced(OPENCASCADE_INCLUDE_DIR OPENCASCADE_LIBRARY)
set(OPENCASCADE_INCLUDE_DIRS ${OPENCASCADE_INCLUDE_DIR} CACHE PATH "OCC include directory.")
set(OPENCASCADE_LIBRARIES 
    TKFillet
    TKernel
    TKG2d
    TKG3d
    TKMath 
    TKShHealing
    TKBool
    TKBO
    TKBRep 
    TKTopAlgo
    TKGeomAlgo
    TKGeomBase
    TKPrim
    TKFeat

    TKCAF
    TKLCAF
    )

