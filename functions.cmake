function(CheckVersionNumber)
    find_package(Git QUIET)

    ParseVersionNumber(${PROJECT_VERSION})

    if(${GIT_FOUND})
        execute_process(
            COMMAND ${GIT_EXECUTABLE} describe
            RESULT_VARIABLE DESCRIBE_RESULT
            OUTPUT_VARIABLE INPUT_VERSION_INFO 
            OUTPUT_STRIP_TRAILING_WHITESPACE
            ERROR_QUIET
        )
        if(${DESCRIBE_RESULT} STREQUAL "0")
            string(REGEX REPLACE "\n$" "" INPUT_VERSION_INFO ${INPUT_VERSION_INFO})
            # Store present values, since ParseVersionNumber will set these same variable names
            # in the current scope
            set(STORED_PROJECT_VERSION ${PROJECT_VERSION})
            set(STORED_VERSION_MAJOR ${PROJECT_VERSION_MAJOR})
            set(STORED_VERSION_MINOR ${PROJECT_VERSION_MINOR})
            set(STORED_VERSION_PATCH ${PROJECT_VERSION_PATCH})
            set(STORED_VERSION_TWEAK ${PROJECT_VERSION_TWEAK})
            set(STORED_VERSION_NHEAD ${PROJECT_VERSION_NHEAD})
            set(STORED_VERSION_SHA ${PROJECT_VERSION_SHA})

            ParseVersionNumber(${INPUT_VERSION_INFO})
            message("-- -- PROJECT_VERSION_TWEAK = ${PROJECT_VERSION_TWEAK}")
            if (NOT "${STORED_VERSION_MAJOR}" STREQUAL "${PROJECT_VERSION_MAJOR}" OR
                NOT "${STORED_VERSION_MINOR}" STREQUAL "${PROJECT_VERSION_MINOR}" OR
                NOT "${STORED_VERSION_PATCH}" STREQUAL "${PROJECT_VERSION_PATCH}" OR
                NOT "${STORED_VERSION_TWEAK}" STREQUAL "${PROJECT_VERSION_TWEAK}"
                )
                message("-- (see error below) PROJECT_VERSION = ${STORED_PROJECT_VERSION}, git tag version = ${INPUT_VERSION_INFO}")
                message(FATAL_ERROR "Provided PROJECT_VERSION does not match latest git tag.  Please update PROJECT_VERSION to match the latest git tag.")
            elseif(("${STORED_VERSION_NHEAD}" STREQUAL "") OR ("${STORED_VERSION_NHEAD}" LESS ${PROJECT_VERSION_NHEAD}))
                message("--> PROJECT_VERSION_SHA information is old. Updating PROJECT_VERSION from ${STORED_PROJECT_VERSION} to ${PROJECT_VERSION} <--")
            elseif("${STORED_VERSION_NHEAD}" GREATER ${PROJECT_VERSION_NHEAD})
                message("-- (see error below) PROJECT_VERSION = ${STORED_PROJECT_VERSION}, git tag version = ${INPUT_VERSION_INFO}")
                message(FATAL_ERROR "SHA info in provided PROJECT_VERSION is newer than in `git describe`. Please check your value.")
            endif()
            set(PROJECT_VERSION ${PROJECT_VERSION} PARENT_SCOPE)
        else()
            message("Git repo not found. Using cached version number, ${PROJECT_VERSION}")
        endif()
    else(${GIT_FOUND})
        message("Git not found. Using cached version number, ${PROJECT_VERSION}")
    endif(${GIT_FOUND})

    # Add the parsed information to the PARENT_SCOPE, since we set originally in
    # ParseVersionNumber function
    set(PROJECT_VERSION_MAJOR ${PROJECT_VERSION_MAJOR} PARENT_SCOPE)
    set(PROJECT_VERSION_MINOR ${PROJECT_VERSION_MINOR} PARENT_SCOPE)
    set(PROJECT_VERSION_PATCH ${PROJECT_VERSION_PATCH} PARENT_SCOPE)
    set(PROJECT_VERSION_TWEAK ${PROJECT_VERSION_TWEAK} PARENT_SCOPE)
    set(PROJECT_VERSION_NHEAD ${PROJECT_VERSION_NHEAD} PARENT_SCOPE)
    set(PROJECT_VERSION_SHA ${PROJECT_VERSION_SHA} PARENT_SCOPE)
    set(PROJECT_VERSION_NOSHA ${PROJECT_VERSION_NOSHA} PARENT_SCOPE)

endfunction(CheckVersionNumber)

function(ParseVersionNumber INPUT_VERSION_INFO)
    # Will set the following variables in the parent scope:
    # PROJECT_VERSION_MAJOR, X in X.Y.Z.W-U-ABC
    # PROJECT_VERSION_MINOR, Y in X.Y.Z.W-U-ABC
    # PROJECT_VERSION_PATCH, Z in X.Y.Z.W-U-ABC
    # PROJECT_VERSION_TWEAK, W in X.Y.Z.W-U-ABC
    # PROJECT_VERSION_NHEAD, U in X.Y.Z.W-U-ABC
    # PROJECT_VERSION_SHA, ABC in X.Y.Z.W-U-ABC

    set(_PATTERN_VERSION "^v?([0-9])+\\.([0-9])+.*")
    set(_PATTERN_PATCH "^v?[0-9]+\\.[0-9]+\\.([0-9])+.*")
    set(_PATTERN_TWEAK "^v?[0-9]+\\.[0-9]+\\.[0-9]+\\.([0-9])+.*")
    set(_PATTERN_SHA "^v?[0-9]+\\.[0-9]+(\\.([0-9])+)?(\\.([0-9])+)?-([0-9]+)-([A-Za-z0-9]+).*")
    string(REGEX REPLACE "${_PATTERN_VERSION}" "\\1" PROJECT_VERSION_MAJOR "${INPUT_VERSION_INFO}")
    string(REGEX REPLACE "${_PATTERN_VERSION}" "\\2" PROJECT_VERSION_MINOR "${INPUT_VERSION_INFO}")
    if(${INPUT_VERSION_INFO} MATCHES ${_PATTERN_PATCH})
        string(REGEX REPLACE "${_PATTERN_PATCH}" "\\1" PROJECT_VERSION_PATCH "${INPUT_VERSION_INFO}")
    else()
        set(PROJECT_VERSION_PATCH "")
    endif()
    if(${INPUT_VERSION_INFO} MATCHES ${_PATTERN_TWEAK})
        string(REGEX REPLACE "${_PATTERN_TWEAK}" "\\1" PROJECT_VERSION_TWEAK "${INPUT_VERSION_INFO}")
    else()
        set(PROJECT_VERSION_TWEAK "")
    endif()
    if(${INPUT_VERSION_INFO} MATCHES ${_PATTERN_SHA})
        # number of commits ahead of tag
        string(REGEX REPLACE "${_PATTERN_SHA}" "\\5" PROJECT_VERSION_NHEAD "${INPUT_VERSION_INFO}") 
        # SHA of current commit
        string(REGEX REPLACE "${_PATTERN_SHA}" "\\6" PROJECT_VERSION_SHA "${INPUT_VERSION_INFO}")
    else()
        set(${PROJECT_VERSION_NHEAD} "")
        set(${PROJECT_VERSION_SHA} "")
    endif()

    set(PROJECT_VERSION "${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}")
    if(NOT ${PROJECT_VERSION_PATCH} STREQUAL "")
        set(PROJECT_VERSION "${PROJECT_VERSION}.${PROJECT_VERSION_PATCH}")
        if(NOT ${PROJECT_VERSION_TWEAK} STREQUAL "")
            set(PROJECT_VERSION "${PROJECT_VERSION}.${PROJECT_VERSION_TWEAK}")
        endif()
    endif()
    set(PROJECT_VERSION_NOSHA ${PROJECT_VERSION})
    if(NOT ${PROJECT_VERSION_NHEAD} STREQUAL "")
        set(PROJECT_VERSION "${PROJECT_VERSION}-${PROJECT_VERSION_NHEAD}-${PROJECT_VERSION_SHA}")
    endif()

    set(PROJECT_VERSION "${PROJECT_VERSION}" PARENT_SCOPE)
    set(PROJECT_VERSION_NOSHA "${PROJECT_VERSION_NOSHA}" PARENT_SCOPE)
    set(PROJECT_VERSION_MAJOR "${PROJECT_VERSION_MAJOR}" PARENT_SCOPE)
    set(PROJECT_VERSION_MINOR "${PROJECT_VERSION_MINOR}" PARENT_SCOPE)
    set(PROJECT_VERSION_PATCH "${PROJECT_VERSION_PATCH}" PARENT_SCOPE)
    set(PROJECT_VERSION_TWEAK "${PROJECT_VERSION_TWEAK}" PARENT_SCOPE)
    set(PROJECT_VERSION_NHEAD "${PROJECT_VERSION_NHEAD}" PARENT_SCOPE)
    set(PROJECT_VERSION_SHA "${PROJECT_VERSION_SHA}" PARENT_SCOPE)

endfunction(ParseVersionNumber)

function(AddTest SrcName)
    add_executable(${SrcName} "${SrcName}.cpp")
    target_link_libraries(${SrcName} "gtest_main" ${ARGN})
    add_test(NAME "TEST_${SrcName}" COMMAND $<TARGET_FILE:${SrcName}>)
endfunction(AddTest)

function(AddLib SrcName SrcFiles)
    add_library(${SrcName} SHARED ${SrcFiles})
    target_link_libraries(${SrcName} ${ARGN})
    set_target_properties(${SrcName} 
        PROPERTIES 
        VERSION ${PROJECT_VERSION_NOSHA}
        SOVERSION "${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}"
    )

    install(TARGETS ${SrcName}
        LIBRARY DESTINATION "${CMAKE_INSTALL_LIBDIR}"
        ARCHIVE DESTINATION "${CMAKE_INSTALL_LIBDIR}"
        PRIVATE_HEADER DESTINATION "${_TARGET_INCLUDE_DIR}"
        PUBLIC_HEADER DESTINATION "${_TARGET_INCLUDE_DIR}"
    )

endfunction(AddLib)

function(AddPyTest SrcName)
endfunction(AddPyTest)

function(InstallHeader FileName)
install(
    FILES "${PROJECT_SOURCE_DIR}/include/${FileName}"
    DESTINATION "${_TARGET_INCLUDE_DIR}"
)
endfunction(InstallHeader)

function(GCC_NoMaybeInitialized TNAME)
    if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
        set_target_properties(${TNAME} PROPERTIES COMPILE_FLAGS "-Wno-maybe-uninitialized")
    endif()
endfunction()

function(GCC_NoCastFunctionType TNAME)
    if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU" AND
       (CMAKE_CXX_COMPILER_VERSION VERSION_GREATER "8" OR
        CMAKE_CXX_COMPILER_VERSION STREQUAL "8"))
            set_target_properties(Py${TNAME} PROPERTIES COMPILE_FLAGS "-Wno-error=cast-function-type")
    endif()
endfunction()

function(UpdateTargetIncludeDir INC_SUFFIX)
    set(_TARGET_INCLUDE_DIR ${CMAKE_INSTALL_INCLUDEDIR})
    if (NOT ${INC_SUFFIX} STREQUAL "")
        file(TO_NATIVE_PATH 
            "${CMAKE_INSTALL_INCLUDEDIR}/${INC_SUFFIX}"
             _TARGET_INCLUDE_DIR)
    endif()
    set(_TARGET_INCLUDE_DIR ${_TARGET_INCLUDE_DIR} PARENT_SCOPE)
endfunction(UpdateTargetIncludeDir)
